#!/usr/bin/env python3
import logging
from lib.webapp import AddMacHandler, RemoveMacHandler
from tornado import web, escape, ioloop, httpclient, gen

app = web.Application([
  (r"/add-device/(.*)", AddMacHandler),
  (r"/remove-device/(.*)", RemoveMacHandler)
])

if __name__ == '__main__':
  port = 8888

  try:
    logging.getLogger('tornado.access').disabled = True
    app.listen(port, address='127.0.0.1')
    ioloop.IOLoop.instance().start()
  except KeyboardInterrupt:
    print("Bye.")
  except Exception as e:
    print("Unknown exception error: ", e)