import routeros_api

class Router:
    def __init__(self, cfg):
        self.host = cfg['Router']['address']
        self.login = cfg['Router']['login']
        self.password = cfg['Router']['password']

        try:
            connection = routeros_api.RouterOsApiPool(self.host, username=self.login, password=self.password)
            self.api = connection.get_api()
        except Exception as e:
            print("Mikrotik connect error")
    
    def __getFW(self):
        try:
            fw_filter = self.api.get_resource('/ip/firewall/filter')
            return fw_filter
        except Exception:
            print("Error getting filter rules")
    def __findJump(self):
        rules = self.__getFW()

        for rule in rules.get():
            if rule['action'] == 'jump':
                return rule['id']
        else:
            return None
    def addMAC(self, mac):
        rules = self.__getFW()
        place_to_insert = self.__findJump()

        try:
            for rule in rules.get():
                if 'src-mac-address' in rule and rule['src-mac-address'].upper() == mac.upper():
                    return 302
            else:
                rules.add(chain='forward', 
                            action='accept', 
                            in_interface='ether2',
                            src_mac_address=mac,
                            place_before=place_to_insert)
                return 200
        except Exception as e:
            return 500
    
    def removeMAC(self,mac):
        rules = self.__getFW()

        try:
            for rule in rules.get():
                if 'src-mac-address' in rule and rule['src-mac-address'] == mac.upper():
                    rules.remove(id=rule['id'])
                    return 200
            else:
                return 404
        except Exception:
            return 500