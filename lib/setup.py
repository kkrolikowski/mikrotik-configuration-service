import logging
from systemd.journal import JournalHandler
import configparser
import sys

class Config:
    def __init__(self):
        self.cfg = configparser.ConfigParser()
    
    def readConfig(self):
        try:
            self.cfg.read(sys.argv[1])
            return self.cfg
        except Exception:
            raise ValueError("Error reading configuration file")

class SystemdLogger:
    def __init__(self):
        self.logger = logging.getLogger(__name__)
        self.__journald_handler = JournalHandler()
        self.__journald_handler.setFormatter(logging.Formatter(
            '[%(levelname)s] %(message)s'
        ))
        self.logger.addHandler(self.__journald_handler)
    
    def start(self):
        return self.logger