import sys
import configparser
import logging
from .router import Router
from .setup import Config, SystemdLogger
from tornado import web, escape, ioloop, httpclient, gen
from datetime import datetime

cfg = Config().readConfig()

mt = Router(cfg)
now = datetime.now()
timestamp = now.strftime("%Y-%m-%d %H:%M:%S")

logger = SystemdLogger().start()
logger.setLevel(logging.INFO)
logger.info("Webservice started at: %s port", cfg['SERVICE']['http_port'])

class AddMacHandler(web.RequestHandler):
    SUPPORTED_METHODS = ("GET")

    def get(self, mac):
        try:
            status = mt.addMAC(mac)
            if status == 200:
                response = {
                    'status': 'DEVICE_ADDED'
                }
                self.set_status(200)
                self.write(response)
                logger.info("Device: %s added", mac)
            elif status == 302:
                response = {
                    'status': 'DEVICE_EXISTS'
                }
                self.set_status(302)
                self.write(response)
                logger.info("Device: %s already exists", mac)
        except Exception as e:
            response =  {
                'error': e.args[0]
            }
            self.set_status(500)
            self.write(response)
            print(timestamp, "Error adding device:", mac)

class RemoveMacHandler(web.RequestHandler):
    SUPPORTED_METHODS = ("GET")

    def get(self, mac):
        try:
            status = mt.removeMAC(mac)
            if status == 200:
                response = {
                    'staus': 'DEVICE_REMOVED'
                }
                self.set_status(200)
                self.write(response)
                print(timestamp, "Device:", mac, "removed")
            elif status == 404:
                response = {
                    'status': 'DEVICE_NOT_FOUND'
                }
                self.set_status(404)
                self.write(response)
                print(timestamp, "Device:", mac, "doesn't exist")
        except Exception as e:
            response =  {
                'error': e.args[0]
            }
            self.set_status(500)
            self.write(response)
            print(timestamp, "Error removing device:", mac)
